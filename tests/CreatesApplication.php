<?php
 /**
 * Filename: CreatesApplication.php
 * Last modified: 2/4/20, 3:56 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

 namespace Tests;

 use Illuminate\Contracts\Console\Kernel;

 trait CreatesApplication {
  /**
   * Creates the application.
   *
   * @return \Illuminate\Foundation\Application
   */
  public function createApplication() {
   $app = require __DIR__ . '/../bootstrap/app.php';

   $app->make(Kernel::class)->bootstrap();

   return $app;
  }
 }
