<?php
 /**
 * Filename: TestCase.php
 * Last modified: 2/4/20, 3:57 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

 namespace Tests;

 use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

 abstract class TestCase extends BaseTestCase {
  use CreatesApplication;
 }
