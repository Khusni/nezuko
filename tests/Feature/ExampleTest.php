<?php
 /**
 * Filename: ExampleTest.php
 * Last modified: 2/4/20, 4:03 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

 namespace Tests\Feature;

 use Illuminate\Foundation\Testing\RefreshDatabase;
 use Tests\TestCase;

 class ExampleTest extends TestCase {
  /**
   * A basic test example.
   *
   * @return void
   */
  public function testBasicTest() {
   $response = $this->get('/');

   $response->assertStatus(302);
  }
 }
