<?php

 use Illuminate\Database\Migrations\Migration;
 use Illuminate\Database\Schema\Blueprint;
 use Illuminate\Support\Facades\Schema;

 class CreateMembersTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
   Schema::create('members', function(Blueprint $table) {
    $table->bigIncrements('id');
    $table->string('name');
    $table->string('email')->unique();
    $table->date('date_of_birth');
    $table->string('working_status');
    $table->text('address');
    $table->string('phone')->unique();
    $table->timestamps();
   });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
   Schema::dropIfExists('members');
  }
 }
