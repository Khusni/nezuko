<?php
 /**
 * Filename: 2019_08_19_000000_create_failed_jobs_table.php
 * Last modified: 2/2/20, 4:57 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

 use Illuminate\Database\Migrations\Migration;
 use Illuminate\Database\Schema\Blueprint;
 use Illuminate\Support\Facades\Schema;

 class CreateFailedJobsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
   Schema::create('failed_jobs', function(Blueprint $table) {
    $table->bigIncrements('id');
    $table->text('connection');
    $table->text('queue');
    $table->longText('payload');
    $table->longText('exception');
    $table->timestamp('failed_at')->useCurrent();
   });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
   Schema::dropIfExists('failed_jobs');
  }
 }
