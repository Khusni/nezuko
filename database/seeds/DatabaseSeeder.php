<?php
 /**
 * Filename: DatabaseSeeder.php
 * Last modified: 2/2/20, 5:03 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

 use Illuminate\Database\Seeder;

 class DatabaseSeeder extends Seeder {
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run() {
   $this->call(UsersTableSeeder::class);
   $this->call(MembersTableSeeder::class);
  }
 }
