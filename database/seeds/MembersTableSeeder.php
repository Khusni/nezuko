<?php

 use Illuminate\Database\Seeder;

 class MembersTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
   $members = [];

   $faker = \Faker\Factory::create('id_ID');

   for ($i = 0; $i < 200; $i++) {
    $members[$i] = [
     'name'           => $faker->unique(true, 1000)->name,
     'email'          => $faker->unique(true, 1000)->safeEmail,
     'date_of_birth'  => $faker->unique(true, 1000)->dateTimeBetween('-30 years',
      '-20 years', 'Asia/Jakarta'),
     'working_status' => $faker->randomElement([
                                                'Belum / Tidak Bekerja',
                                                'Pelajar / Mahasiswa',
                                                'Karyawan Swasta',
                                                'Karyawan Honorer',
                                                'Pegawai Negeri Sipil',
                                                'Dosen',
                                                'Guru',
                                               ]),
     'address'        => $faker->address,
     'phone'          => $faker->unique(true, 1000)->phoneNumber,
     'created_at'     => \Carbon\Carbon::now(),
     'updated_at'     => \Carbon\Carbon::now(),
    ];
   }

   DB::table('members')->truncate();
   DB::table('members')->insert($members);
  }
 }
