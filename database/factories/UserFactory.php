<?php
 /**
  * Filename: UserFactory.php
  * Last modified: 2/2/20, 6:50 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 /** @var \Illuminate\Database\Eloquent\Factory $factory */

 use App\Models\User;
 use Faker\Generator as Faker;
 use Illuminate\Support\Str;

 /*
 |--------------------------------------------------------------------------
 | Model Factories
 |--------------------------------------------------------------------------
 |
 | This directory should contain each of the model factory definitions for
 | your application. Factories provide a convenient way to generate new
 | model instances for testing / seeding your application's database.
 |
 */

 $factory->define(User::class, function(Faker $faker) {
  return [
   'name'              => $faker->name,
   'email'             => $faker->unique()->safeEmail,
   'email_verified_at' => now(),
   'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
   // password
   'remember_token'    => Str::random(10),
  ];
 });
