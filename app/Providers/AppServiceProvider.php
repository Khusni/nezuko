<?php
 /**
  * Filename: AppServiceProvider.php
  * Last modified: 2/2/20, 6:49 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Providers;

 use Illuminate\Support\ServiceProvider;

 class AppServiceProvider extends ServiceProvider {
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
   //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
   //
  }
 }
