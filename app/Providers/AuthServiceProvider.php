<?php
 /**
  * Filename: AuthServiceProvider.php
  * Last modified: 2/2/20, 6:49 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Providers;

 use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
 use Illuminate\Support\Facades\Gate;

 class AuthServiceProvider extends ServiceProvider {
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
   // 'App\Model' => 'App\Policies\ModelPolicy',
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot() {
   $this->registerPolicies();
   //
  }
 }
