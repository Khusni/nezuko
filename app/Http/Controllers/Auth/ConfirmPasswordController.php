<?php
 /**
  * Filename: ConfirmPasswordController.php
  * Last modified: 2/2/20, 6:49 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Http\Controllers\Auth;

 use App\Http\Controllers\Controller;
 use App\Providers\RouteServiceProvider;
 use Illuminate\Foundation\Auth\ConfirmsPasswords;

 class ConfirmPasswordController extends Controller {
  /*
  |--------------------------------------------------------------------------
  | Confirm Password Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for handling password confirmations and
  | uses a simple trait to include the behavior. You're free to explore
  | this trait and override any functions that require customization.
  |
  */

  use ConfirmsPasswords;

  /**
   * Where to redirect users when the intended url fails.
   *
   * @var string
   */
  protected $redirectTo = RouteServiceProvider::HOME;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
   $this->middleware('auth');
  }
 }
