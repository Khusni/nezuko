<?php
 /**
  * Filename: AjaxController.php
  * Last modified: 2/4/20, 4:51 PM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020 Nezuko
  */

 namespace App\Http\Controllers;

 use App\Models\Member;
 use Illuminate\Http\Request;

 class AjaxController extends Controller {
  /**
   * Instantiate a new controller instance.
   *
   * @return void
   */
  public function __construct() {
   $this->middleware([
    'auth',
    'ajax'
   ]);
  }

  public function getPage(Request $request) {
   $members = Member::paginate(10);

   if ($request->ajax()) {
    return view('member', compact('members'));
   }
  }
 }
