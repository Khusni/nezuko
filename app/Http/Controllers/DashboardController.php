<?php
 /**
  * Filename: DashboardController.php
  * Last modified: 2/3/20, 8:56 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Http\Controllers;

 use App\Models\Member;
 use Illuminate\Http\Request;

 class DashboardController extends Controller {
  public function showDashboard() {
   $members = Member::paginate(10);

   return View('dashboard', compact('members'));
  }
 }
