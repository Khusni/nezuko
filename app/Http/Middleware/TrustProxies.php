<?php
 /**
  * Filename: TrustProxies.php
  * Last modified: 2/2/20, 6:49 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Http\Middleware;

 use Fideloper\Proxy\TrustProxies as Middleware;
 use Illuminate\Http\Request;

 class TrustProxies extends Middleware {
  /**
   * The trusted proxies for this application.
   *
   * @var array|string
   */
  protected $proxies;

  /**
   * The headers that should be used to detect proxies.
   *
   * @var int
   */
  protected $headers = Request::HEADER_X_FORWARDED_ALL;
 }
