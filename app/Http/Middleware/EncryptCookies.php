<?php
 /**
  * Filename: EncryptCookies.php
  * Last modified: 2/2/20, 6:49 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Http\Middleware;

 use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

 class EncryptCookies extends Middleware {
  /**
   * The names of the cookies that should not be encrypted.
   *
   * @var array
   */
  protected $except = [
   //
  ];
 }
