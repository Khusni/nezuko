<?php
 /**
  * Filename: AjaxRequestOnly.php
  * Last modified: 2/3/20, 8:15 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Http\Middleware;

 use Closure;

 class AjaxRequestOnly {
  /**
   * Handle an incoming request.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Closure $next
   *
   * @return mixed
   */
  public function handle($request, Closure $next) {
   if ($request->ajax()) {
    return $next($request);
   }

   abort(403, 'Unauthorized Access!');
  }
 }
