<?php
 /**
  * Filename: CheckForMaintenanceMode.php
  * Last modified: 2/2/20, 6:49 AM
  *
  * @author Panji Setya Nur Prawira
  * @facebook https://www.facebook.com/PanjiNamjaElf
  * @twitter https://twitter.com/PanjiNamjaElf18
  * @github https://github.com/PanjiNamjaElf/
  * @gitlab https://gitlab.com/PanjiNamjaElf
  * @copyright (c) 2020
  */

 namespace App\Http\Middleware;

 use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

 class CheckForMaintenanceMode extends Middleware {
  /**
   * The URIs that should be reachable while maintenance mode is enabled.
   *
   * @var array
   */
  protected $except = [
   //
  ];
 }
