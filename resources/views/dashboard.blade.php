@extends('layout.default')

@section('content')
 <!-- Page-Title -->
 <div class="row">
  <div class="col-sm-12">
   <div class="btn-group float-right m-b-15">
    <a href="#" class="btn btn-danger">Tambah Anggota</a>
   </div>

   <h4 class="page-title">Daftar Anggota</h4>
  </div>
 </div>

 <div id="member-table" class="table-responsive-sm">
  @include('member')
 </div>
@endsection