<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Nezuko') }}</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Bootstrap -->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

  @auth()
   <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">

   @if (Route::is('dashboard'))
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
   @endif

   <script src="{{ asset('js/fontawesome-solid-5.0.13.js') }}"></script>
   <script src="{{ asset('js/fontawesome-5.0.13.js') }}"></script>
  @endauth
 </head>

 <body>
  @guest()
   <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <div class="container">
     <a class="navbar-brand" href="{{ url('/') }}">
      {{ config('app.name', 'Nezuko') }}
     </a>
    </div>
   </nav>

   <main class="py-4">
    @yield('content')
   </main>

  @else
   <div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
     <div class="sidebar-header">
      <h3>Nezuko</h3>
      <strong>N</strong>
     </div>

     <ul class="list-unstyled components">
      <li{!! Route::is('dashboard') ? ' class="active"' : '' !!}>
       <a href="{{ route('dashboard') }}"><i class="fas fa-caret-right"></i> Dasbor</a>
      </li>

      <li>
       <a href="#"><i class="fas fa-caret-right"></i> Tambah Simpanan</a>
      </li>

      <li>
       <a href="#"><i class="fas fa-caret-right"></i> Tarik Simpanan</a>
      </li>
     </ul>
    </nav>

    <!-- Page Content  -->
    <div id="content">
     <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
       <button type="button" id="sidebarCollapse" class="btn btn-info">
        <i class="fas fa-align-left"></i>
        <span>Sidebar</span>
       </button>

       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
       </button>

       <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav navbar-nav ml-auto">
         <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
             aria-haspopup="true" aria-expanded="false" v-pre>
           {{ Auth::user()->username }} <span class="caret"></span>
          </a>

          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
           <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();document.getElementById('logout-form').submit();">Keluar</a>

           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
            @csrf
           </form>
          </div>
         </li>
        </ul>
       </div>
      </div>
     </nav>

     @yield('content')
    </div>
   </div>

  @endguest

  <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>

  @auth()
   <script type="text/javascript">
    @if (Route::is('dashboard'))
     $(window).on('hashchange', function() {
      if (window.location.hash) {
       var page = window.location.hash.replace('#', '');

       if (page == Number.NaN || page <= 0) {
        return false;
       }

       else {
        getData(page);
       }
      }
     });
    @endif

    $(document).ready(function() {
     $('#sidebarCollapse').on('click', function() {
      $('#sidebar').toggleClass('active');
     });

     @if (Route::is('dashboard'))
      $(document).on('click', '.pagination a', function(event) {
       event.preventDefault();

       $('#member-table').append(
        '<img src="{{ asset('images/loading.gif') }}" class="ajax-loader">'
       );

       $('li').removeClass('active');
       $(this).parent('li').addClass('active');

       var myurl = $(this).attr('href');
       var page = $(this).attr('href').split('page=')[1];

       getData(page);
      });

      function getData(page) {
       $.ajax({
        url: '{{ route('ajax-get-page') }}/?page=' + page,
        type: 'get',
        datatype: 'html'
       }).done(function(data) {
        $("#member-table").empty().html(data);
        location.hash = page;
       }).fail(function(jqXHR, ajaxOptions, thrownError) {
        alert('No response from server');
       });
      }

      $(".member-profile").click(function() {
       window.location.href = $(this).data("href");
      });
     @endif
    });
   </script>
  @endauth
 </body>
</html>