<table id="list-table" class="table table-bordred table-striped table-hover">
 <thead>
  <tr>
   <th style="width: 6%;">ID</th>
   <th style="width: 49%;">Nama</th>
   <th style="width: 20%;">Tanggal Lahir</th>
   <th style="width: 20%;">Telepon</th>
  </tr>
 </thead>

 <tbody>
  @foreach($members as $member)
   <tr class="member-profile" data-href="#">
    <td>{{ $member->id }}</td>
    <td>{{ $member->name }}</td>
    <td>{{ $member->date_of_birth->format('Y-m-d') }}</td>
    <td>{{ $member->phone }}</td>
   </tr>
  @endforeach
 </tbody>
</table>

{!! $members->render() !!}