<p align="center">
 <img src="https://i.imgur.com/Pm80FNG.png" alt="Nezuko Cover Image">
</p>

<p align="center">
 <a href="http://laravel.com"><img src="https://img.shields.io/badge/Laravel-6-f4645f.svg?style=flat-square" /></a>
 <a href="https://gitlab.com/PanjiNamjaElf/nezuko/pipelines"><img src="https://gitlab.com/PanjiNamjaElf/nezuko/badges/master/pipeline.svg?style=flat" /></a>
</p>

## Table of Contents
1. [Requirements](#requirements)
2. [Installation](#installation)
3. [Demo](#demo)

## <a name="requirements"></a> Requirements
- PHP >= 7.2.0
  - BCMath PHP Extension
  - Ctype PHP Extension
  - JSON PHP Extension
  - Mbstring PHP Extension
  - OpenSSL PHP Extension
  - PDO PHP Extension
  - Tokenizer PHP Extension
  - XML PHP Extension

## <a name="installation"></a> Installation
1. Clone this package using git bash with the following command: <br>
`git clone https://gitlab.com/PanjiNamjaElf/nezuko.git`

2. Copy .env file and fill it with your DB info: <br>
`cd nezuko` <br>
`cp .env.example .env`

3. Install package using composer: <br>
`composer install`

4. Set up <br>
`php artisan config:clear` <br>
`php artisan key:generate` <br>
`php artisan migrate --seed` <br>

5. Run server <br>
`php artisan serve`

6. Enjoy the app at `http://localhost:8000`

## <a name="demo"></a> Demo

URL: https://nezuko-app.herokuapp.com

Username: `PanjiNamjaElf` <br>
Password: `nezuko`
